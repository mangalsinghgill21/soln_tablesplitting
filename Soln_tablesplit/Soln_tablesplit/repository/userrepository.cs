﻿using Soln_tablesplit.Entity;
using Soln_tablesplit.ER;
using Soln_tablesplit.repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_tablesplit.repository
{
    class userrepository : Iuserrepository
    {
        #region Declaration
        private testdbEntities db = null;
        #endregion

        #region Constructor
        public userrepository()
        {
            db = new testdbEntities();
        }
        #endregion

        #region Public Method
        public async Task<IEnumerable<user>> GetUser()
        {
            try
            {
                return await Task.Run(() => {

                    var  getQuery =
                        db
                        .useralls
                        .AsEnumerable()
                        .Select(this.SelectUser)
                        .ToList();

                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion

        #region Private Property
        private Func<userall,user> SelectUser
        {
            get
            {
                return
                    (letbluserobj) => new user()
                    {
                        firstname = letbluserobj.firstname,
                        lastname = letbluserobj.lastname,
                        userid = (int?)letbluserobj.userid,
                        userlogin = new userlogin()
                        {
                            username = letbluserobj.login.username,
                            password = letbluserobj.login.password

                        },
                        usercommunication = new usercommunication()
                        {
                            emailid = letbluserobj.communication.emailid,
                            mobileno = letbluserobj.communication.mobileno
                        }
                    };
            }
        }
        #endregion
    }
}
