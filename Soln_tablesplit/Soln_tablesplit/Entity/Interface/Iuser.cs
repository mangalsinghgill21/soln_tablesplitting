﻿using Soln_tablesplit.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Soln_tablesplit.Interface
{
     public  interface Iuser
    {
      public   int userid { get; set; }

      public  String firstname { get; set; }

     public   String lastname { get; set; }

     public  usercommunication usercommunication { get;set; }

     public   userlogin userlogin { get; set; }
    }
}
